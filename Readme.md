## Installation
****
Le programme a besoin du SAT solver PySAT, pour cela il faut simplement éxécuter la ligne de commande suivante :
```
$ pip install python-sat[pblib,aiger]
```
****
## Lancement
****
Il faut évidemment python3 sur la machine.
Pour lancer la ligne commande :
```
$ python3 Pigeon.py <nombre de pigeons> <nombre de nids>
```
****
## Explication fonctionnement
****
Le programme vous affiche le résultat dans le terminal comme un SAT solveur normal, UNSAT si il n'a pas trouvé de solution, SAT : <model> si il trouve une solution.

De plus vous trouverez après le lancement, le fichier 'pigeon.dimacs' ou le problème est écrit au format CNF avec le résultat à la fin.

****
## Test
****
Pour les test il y a un script bash qui lance toutes les éxécutions avec le nombre de pigeons entre 1 et 7, et le nombre de nids entre 1 et 7.
Pour le lancer dans le terminal :
```
$ bash testPigeon.sh
```
