#!/usr/bin/env python3
from pysat.solvers import Solver
import sys

if(len(sys.argv)!=3):
    print("Erreur nombre d'argument\n")
    sys.exit()

m = int(sys.argv[1])
n = int(sys.argv[2])
variables = []
clauses_totales = []

def inputs(m,n):
    for i in range(m):
        variables.append([])

    cpt_var = 0
    for i in range(0,m):
        for ii in range(n):
            cpt_var = cpt_var+1
            variables[i].append(cpt_var)

def atLeastOne(s):
    for i in range(m): 
        clauses = []
        for ii in range(n):              
            clauses.append(variables[i][ii])
        clauses_totales.append(clauses)
        s.add_clause(clauses)


def atMostOne(s):
    for i in range(n):
        for ii in range(m):
            tmp_i = ii
            
            while tmp_i < m-1:
                clauses = []
                tmp_i = tmp_i+1
                clauses.append(-variables[ii][i])
                clauses.append(-variables[tmp_i][i])
                clauses_totales.append(clauses)
                s.add_clause(clauses)
                
def write_file(s):
    fichier = open("pigeon.dimacs", "w")  
    fichier.write("p cnf "+str(m*n)+" "+str(len(clauses_totales))+"\n")  
    for li in clauses_totales:
        for clause in li:
            fichier.write(str(clause)+" ")
        fichier.write('0\n')
    
    fichier.write("Resultat :"+str(s.get_model()))
    

def main():
    inputs(m,n)
    s = Solver(name='g4')
    atLeastOne(s)
    atMostOne(s)
    if(s.solve()):
        print("SAT :"+str(s.get_model()))
    else:
        print("UNSAT")
    write_file(s)

main()